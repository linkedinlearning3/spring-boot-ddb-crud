package ddb.demo.repository;

import ddb.demo.domain.Employee;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@EnableScan
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, String> {
}